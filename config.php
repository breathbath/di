<?php

use B\DI\DependencyItem\ConfigFields;
use B\Examples\Classes\ClassA;
use B\Examples\Classes\ClassB;
use B\Examples\Classes\ClassC;
use B\Examples\Classes\ClassD;

$exampleConfig =  [
    [
        ConfigFields::CLASS_FIELD => ClassA::class,
        ConfigFields::DEPENDENCIES_FIELD => [
            ClassB::class
        ]
    ],
    [
        ConfigFields::CLASS_FIELD => ClassD::class,
        ConfigFields::DEPENDENCIES_FIELD => [
            ClassA::class,
            ClassC::class
        ]
    ],
    [
        ConfigFields::CLASS_FIELD => ClassB::class,
        ConfigFields::DEPENDENCIES_FIELD => [
            ClassC::class
        ]
    ],
    [
        ConfigFields::CLASS_FIELD => ClassC::class
    ],

];