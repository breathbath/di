<?php
use B\DI\Container;
use B\DI\DependencyItem\ConfigToDependencyCollectionConverter;
use B\Examples\Classes\ClassA;
use B\Examples\Classes\ClassB;
use B\Examples\Classes\ClassC;
use B\Examples\Classes\ClassD;
use B\Examples\DynamicItems\DynamicItemCollectionProviderFactory;

include __DIR__ . '/vendor/autoload.php';
include 'config.php';

$container = new Container(new ConfigToDependencyCollectionConverter($exampleConfig));
$classA = $container->getDependencyByName(ClassA::class);
$classB = $container->getDependencyByName(ClassB::class);
$classC = $container->getDependencyByName(ClassC::class);
$classD = $container->getDependencyByName(ClassD::class);
echo $classA->getName();
echo $classB->getName();
echo $classC->getName();
echo $classD->getName();

echo PHP_EOL;
echo '<br/>';

$dynsmicDependencyItemProviderFactory = new DynamicItemCollectionProviderFactory();
$container = new Container($dynsmicDependencyItemProviderFactory->createDynamicItemCollectionProvider());

$classA = $container->getDependencyByName(ClassA::class);
$classB = $container->getDependencyByName(ClassB::class);
$classC = $container->getDependencyByName(ClassC::class);
$classD = $container->getDependencyByName(ClassD::class);
echo $classA->getName();
echo $classB->getName();
echo $classC->getName();
echo $classD->getName();
