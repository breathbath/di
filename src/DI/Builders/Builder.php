<?php

namespace B\DI\Builders;

use B\DI\DependenciesProvider;

interface Builder
{
    /**
     * @param bool $isSingleTon
     * @return mixed
     */
    public function build($isSingleTon = true);

    /**
     * @return string
     */
    public function getBuildingResultClassName();
}