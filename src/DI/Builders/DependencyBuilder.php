<?php

namespace B\DI\Builders;

use B\DI\DependenciesProvider;
use B\DI\DependencyItem\DependencyItem;

class DependencyBuilder implements Builder
{
    /**
     * @var string[]
     */
    private $dependencyNames;

    /**
     * @var string
     */
    private $expectedClassName;

    /**
     * @var \Closure
     */
    private $closure;

    /**
     * @var mixed
     */
    private $cachedInstance;

    /**
     * @var DependenciesProvider
     */
    private $provider;

    /**
     * DependantBuilder constructor.
     * @param DependencyItem $dependencyItem
     * @param DependenciesProvider $provider
     */
    public function __construct(DependencyItem $dependencyItem, DependenciesProvider $provider)
    {
        $this->dependencyNames = $dependencyItem->getDependencyNames();
        $this->expectedClassName = $dependencyItem->getClass();
        $this->closure = $dependencyItem->getClosure();
        $this->provider = $provider;
    }

    /**
     * @inheritdoc
     */
    public function build($isSingleTon = true)
    {
        if (!$isSingleTon || !$this->cachedInstance) {
            $this->cachedInstance = $this->buildDependency($this->provider);
        }

        return $this->cachedInstance;
    }

    /**
     * @param DependenciesProvider $provider
     * @return mixed
     * @throws \Exception
     */
    private function buildDependency(DependenciesProvider $provider)
    {
        $result = call_user_func_array($this->closure, $this->getDependencyConstructorArguments($provider));
        $className = $this->getBuildingResultClassName();
        if (!$result instanceof $className) {
            throw new \Exception('Wrong dependency created');
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getBuildingResultClassName()
    {
        return $this->expectedClassName;
    }

    /**
     * @param DependenciesProvider $provider
     * @return array
     */
    protected function getDependencyConstructorArguments(DependenciesProvider $provider)
    {
        $arguments = [];

        $dependencyNames = $this->dependencyNames;

        foreach ($dependencyNames as $dependencyName) {
            $arguments[] = $provider->getDependencyByName($dependencyName);
        }

        return $arguments;
    }
}