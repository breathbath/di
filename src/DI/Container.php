<?php

namespace B\DI;

use B\DI\Builders\Builder;
use B\DI\Builders\DependencyBuilder;
use B\DI\DependencyItem\ItemCollectionProvider;

class Container implements DependenciesProvider
{
    /**
     * @var Builder[]
     */
    private $constructors;

    /**
     * Container constructor.
     * @param ItemCollectionProvider $collectionProvider
     */
    public function __construct(ItemCollectionProvider $collectionProvider)
    {
        $dependencyItems = $collectionProvider->getCollection();

        foreach ($dependencyItems as $item) {
            $this->constructors[$item->getClass()] = new DependencyBuilder($item, $this);
        }
    }

    /**
     * @inheritdoc
     */
    public function getDependencyByName($name, $isSingleton = true)
    {
        if (isset($this->constructors[$name])) {
            $builder = $this->constructors[$name];

            return $builder->build($isSingleton);
        }
        throw new \Exception('Unknown dependency ' . $name);
    }
}