<?php

namespace B\DI;

interface DependenciesProvider
{
    /**
     * @param string $name
     * @return mixed
     */
    public function getDependencyByName($name);
}