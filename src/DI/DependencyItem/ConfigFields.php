<?php

namespace B\DI\DependencyItem;

interface ConfigFields
{
    const CLASS_FIELD = 'class';
    const DEPENDENCIES_FIELD = 'dependencies';
}