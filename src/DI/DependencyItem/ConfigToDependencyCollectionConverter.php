<?php

namespace B\DI\DependencyItem;

class ConfigToDependencyCollectionConverter implements ItemCollectionProvider
{
    /**
     * @var array
     */
    private $config;

    /**
     * ConfigItemCollectionProvider constructor.
     * @param $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @return DependencyItemCollection
     * @throws \Exception
     */
    public function getCollection()
    {
        $collection = new DependencyItemCollection();
        foreach ($this->config as $configElement) {
            $collection->attach($this->getItemFromConfigElement($configElement));
        }

        return $collection;
    }

    /**
     * @param array $configElement
     * @return DependencyItemStatic
     */
    private function getItemFromConfigElement(array $configElement)
    {
        $class = $configElement[ConfigFields::CLASS_FIELD];
        $dependencies = (isset($configElement[ConfigFields::DEPENDENCIES_FIELD])) ? $configElement[ConfigFields::DEPENDENCIES_FIELD] : [];
        $dependencyItem = new DependencyItemStatic($class, $dependencies);

        return $dependencyItem;
    }
}