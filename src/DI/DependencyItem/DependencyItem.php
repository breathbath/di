<?php

namespace B\DI\DependencyItem;

interface DependencyItem
{
    /**
     * @return string
     */
    public function getClass();

    /**
     * @return \Closure
     */
    public function getClosure();

    /**
     * @return string[]
     */
    public function getDependencyNames();

}