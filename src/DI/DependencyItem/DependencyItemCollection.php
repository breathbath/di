<?php
/**
 * Created by PhpStorm.
 * User: breathbath
 * Date: 03.10.16
 * Time: 11:18
 */

namespace B\DI\DependencyItem;

class DependencyItemCollection extends \SplObjectStorage
{
    public function attach($object, $data = null)
    {
        if(!$object instanceof DependencyItem) {
            throw new \Exception(DependencyItem::class . ' class is expected');
        }
        parent::attach($object, $data);
    }

    public function addAll($storage)
    {
        if(!$storage instanceof DependencyItemCollection) {
            throw new \Exception(DependencyItemCollection::class . ' class is expected');
        }
        parent::addAll($storage);
    }
}