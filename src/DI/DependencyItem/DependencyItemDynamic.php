<?php

namespace B\DI\DependencyItem;

class DependencyItemDynamic implements DependencyItem
{
    /**
     * @var string
     */
    private $class;

    /**
     * @var string[]
     */
    private $dependencies = [];

    /**
     * @var \Closure
     */
    private $closure;

    /**
     * DependencyItemStatic constructor.
     * @param \Closure $closure
     * @param \string[] $dependencies
     * @param string $class
     */
    public function __construct($class, \Closure $closure, array $dependencies = [])
    {
        $this->class = $class;
        $this->dependencies = $dependencies;
        $this->closure = $closure;
    }

    public function getClass()
    {
        return $this->class;
    }

    public function getClosure()
    {
        return $this->closure;
    }

    public function getDependencyNames()
    {
        return $this->dependencies;
    }
}