<?php

namespace B\DI\DependencyItem;

use ReflectionClass;

class DependencyItemStatic implements DependencyItem
{
    /**
     * @var string
     */
    private $class;

    /**
     * @var string[]
     */
    private $dependencies;

    /**
     * DependencyItemStatic constructor.
     * @param string $class
     * @param \string[] $dependencies
     */
    public function __construct($class, array $dependencies = [])
    {
        $this->class = $class;
        $this->dependencies = $dependencies;
    }


    public function getClass()
    {
        return $this->class;
    }

    public function getClosure()
    {
        return function() {
            $reflect  = new ReflectionClass($this->class);
            $instance = $reflect->newInstanceArgs(func_get_args());
            return $instance;
        };
    }

    /**
     * @return string[]
     */
    public function getDependencyNames()
    {
        return $this->dependencies;
    }
}