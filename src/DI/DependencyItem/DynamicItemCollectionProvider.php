<?php

namespace B\DI\DependencyItem;

class DynamicItemCollectionProvider implements ItemCollectionProvider
{
    /**
     * @var DependencyItemCollection
     */
    private $dynamicItems;

    /**
     * DynamicItemCollectionProvider constructor.
     */
    public function __construct()
    {
        $this->dynamicItems = new DependencyItemCollection();
    }

    /**
     * @inheritdoc
     */
    public function getCollection()
    {
        return $this->dynamicItems;
    }

    /**
     * @param DynamicItemFactory $itemDynamicFactory
     */
    public function addDynamicItemFactory(DynamicItemFactory $itemDynamicFactory)
    {
        $this->dynamicItems->attach($itemDynamicFactory->createDynamicItem());
    }
}