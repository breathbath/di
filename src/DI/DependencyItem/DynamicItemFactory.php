<?php

namespace B\DI\DependencyItem;

interface DynamicItemFactory
{
    /**
     * @return DependencyItemDynamic
     */
    public function createDynamicItem();
}