<?php

namespace B\DI\DependencyItem;

interface ItemCollectionProvider
{
    /**
     * @return DependencyItemCollection
     */
    public function getCollection();
}