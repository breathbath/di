<?php

namespace B\Examples\Classes;

class ClassA extends NamedClass
{
    /**
     * @var ClassB
     */
    private $classB;

    /**
     * ClassA constructor.
     * @param ClassB $classB
     */
    public function __construct(ClassB $classB)
    {
        $this->classB = $classB;
    }

    public function getName()
    {
        return 'Class A';
    }
}