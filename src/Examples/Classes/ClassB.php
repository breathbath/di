<?php

namespace B\Examples\Classes;

class ClassB extends NamedClass
{
    /**
     * @var ClassC
     */
    private $classC;

    /**
     * ClassB constructor.
     * @param ClassC $classC
     */
    public function __construct(ClassC $classC)
    {
        $this->classC = $classC;
    }

    public function getName()
    {
        return 'Class B';
    }
}