<?php

namespace B\Examples\Classes;

class ClassC extends NamedClass
{
    public function getName()
    {
        return 'Class C';
    }
}