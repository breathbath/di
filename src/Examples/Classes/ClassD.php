<?php

namespace B\Examples\Classes;

class ClassD extends NamedClass
{
    /**
     * @var ClassA
     */
    private $classA;

    /**
     * @var ClassC
     */
    private $classC;

    /**
     * ClassD constructor.
     * @param ClassA $classA
     * @param ClassC $classC
     */
    public function __construct(ClassA $classA, ClassC $classC)
    {
        $this->classA = $classA;
        $this->classC = $classC;
    }

    public function getName()
    {
        return 'Class D';
    }

}