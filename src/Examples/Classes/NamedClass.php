<?php

namespace B\Examples\Classes;

abstract class NamedClass
{
    /**
     * @return string
     */
    abstract public function getName();
}