<?php
namespace B\Examples\DynamicItems;

use B\DI\DependencyItem\DependencyItemDynamic;
use B\DI\DependencyItem\DynamicItemFactory;
use B\Examples\Classes\ClassA;
use B\Examples\Classes\ClassB;

class ClassAFactory implements DynamicItemFactory
{
    /**
     * @inheritdoc
     */
    public function createDynamicItem()
    {
        return new DependencyItemDynamic(
            ClassA::class,
            function(ClassB $classB) {
                return new ClassA($classB);
            },
            [ClassB::class]
        );
    }
}