<?php
namespace B\Examples\DynamicItems;

use B\DI\DependencyItem\DependencyItemDynamic;
use B\DI\DependencyItem\DynamicItemFactory;
use B\Examples\Classes\ClassB;
use B\Examples\Classes\ClassC;

class ClassBFactory implements DynamicItemFactory
{
    /**
     * @inheritdoc
     */
    public function createDynamicItem()
    {
        return new DependencyItemDynamic(
            ClassB::class,
            function(ClassC $classC) {
                return new ClassB($classC);
            },
            [ClassC::class]
        );
    }
}