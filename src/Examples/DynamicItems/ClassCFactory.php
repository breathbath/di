<?php
namespace B\Examples\DynamicItems;

use B\DI\DependencyItem\DependencyItemDynamic;
use B\DI\DependencyItem\DynamicItemFactory;
use B\Examples\Classes\ClassB;
use B\Examples\Classes\ClassC;

class ClassCFactory implements DynamicItemFactory
{
    /**
     * @inheritdoc
     */
    public function createDynamicItem()
    {
        return new DependencyItemDynamic(
            ClassC::class,
            function() {
                return new ClassC();
            }
        );
    }
}