<?php
namespace B\Examples\DynamicItems;

use B\DI\DependencyItem\DependencyItemDynamic;
use B\DI\DependencyItem\DynamicItemFactory;
use B\Examples\Classes\ClassA;
use B\Examples\Classes\ClassC;
use B\Examples\Classes\ClassD;

class ClassDFactory implements DynamicItemFactory
{
    /**
     * @inheritdoc
     */
    public function createDynamicItem()
    {
        return new DependencyItemDynamic(
            ClassD::class,
            function(ClassA $classA, ClassC $classC) {
                return new ClassD($classA, $classC);
            },
            [ClassA::class, ClassC::class]
        );
    }
}