<?php

namespace B\Examples\DynamicItems;

use B\DI\DependencyItem\DynamicItemCollectionProvider;

class DynamicItemCollectionProviderFactory
{
    public function createDynamicItemCollectionProvider()
    {
        $dynamicItemCollection = new DynamicItemCollectionProvider();

        $dynamicItemCollection->addDynamicItemFactory(new ClassAFactory());
        $dynamicItemCollection->addDynamicItemFactory(new ClassBFactory());
        $dynamicItemCollection->addDynamicItemFactory(new ClassCFactory());
        $dynamicItemCollection->addDynamicItemFactory(new ClassDFactory());

        return $dynamicItemCollection;
    }
}