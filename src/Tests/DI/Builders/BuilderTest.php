<?php
namespace B\Tests\Builders;

use B\DI\Builders\DependencyBuilder;
use B\DI\DependenciesProvider;
use B\DI\DependencyItem\DependencyItem;
use B\Examples\Classes\ClassA;
use B\Examples\Classes\ClassB;
use B\Examples\Classes\ClassC;
use Prophecy\Prophecy\ObjectProphecy;
use Prophecy\Prophet;

class BuilderTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var DependenciesProvider|ObjectProphecy
     */
    private $dependenciesProvider;

    protected function setUp()
    {
        $this->dependenciesProvider = $this->prophesize(DependenciesProvider::class);
    }

    public function testCreation()
    {
        $classBDummy = $this->prophesize(ClassB::class)->reveal();
        $this->dependenciesProvider->getDependencyByName(ClassB::class)->willReturn($classBDummy);

        $classAItem = $this->createDependencyItem(
            ClassA::class,
            function (ClassB $classB) {
                return new ClassA($classB);
            },
            [ClassB::class]
        );

        $builder = $this->createBuilder($classAItem);
        $classA = $builder->build();
        $this->assertInstanceOf(ClassA::class, $classA);
    }

    public function testSingleton()
    {
        $classCItem = $this->createDependencyItem(
            ClassC::class,
            function () {
                return new ClassC();
            },
            []
        );
        $builder = $this->createBuilder($classCItem);

        $item1 = $builder->build();
        $item2 = $builder->build(true);
        $item3 = $builder->build(false);

        $this->assertTrue($item1 === $item2);
        $this->assertFalse($item3 === $item1);
        $this->assertFalse($item3 === $item2);
    }

    public function testResultMismatch()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Wrong dependency created');

        $classCItem = $this->createDependencyItem(
            ClassC::class,
            function () {
                return new ClassB(new ClassC());
            },
            []
        );

        $builder = $this->createBuilder($classCItem);
        $builder->build();
    }

    private function createBuilder(DependencyItem $dependencyItem)
    {
        $builder = new DependencyBuilder($dependencyItem, $this->dependenciesProvider->reveal());
        return $builder;
    }

    /**
     * @param string $class
     * @param \Closure $closure
     * @param array $dependencyNames
     * @return DependencyItem|ObjectProphecy
     */
    private function createDependencyItem($class, $closure, $dependencyNames = [])
    {
        /** @var DependencyItem|ObjectProphecy $item */
        $item = $this->prophesize(DependencyItem::class);
        $item->getClosure()->willReturn($closure);
        $item->getClass()->willReturn($class);
        $item->getDependencyNames()->willReturn($dependencyNames);

        return $item->reveal();
    }
}