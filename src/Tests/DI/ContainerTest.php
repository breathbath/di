<?php

namespace B\Tests\DI;

use B\DI\Container;
use B\DI\DependencyItem\DependencyItem;
use B\DI\DependencyItem\DependencyItemCollection;
use B\DI\DependencyItem\ItemCollectionProvider;
use B\Examples\Classes\ClassC;

class ContainerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Container
     */
    private $container;

    protected function setUp()
    {
        $item = $this->prophesize(DependencyItem::class);
        $item->getClass()->willReturn(ClassC::class);
        $item->getClosure()->willReturn(
            function(){
                return new ClassC();
            }
        );
        $item->getDependencyNames()->willReturn([]);


        $itemCollection = new DependencyItemCollection();
        $itemCollection->attach($item->reveal());

        $itemCollectionProvider = $this->prophesize(ItemCollectionProvider::class);
        $itemCollectionProvider->getCollection()->willReturn($itemCollection);

        $this->container = new Container($itemCollectionProvider->reveal());
    }

    public function testDependencyResolving()
    {
        $class = $this->container->getDependencyByName(ClassC::class);
        $this->assertInstanceOf(ClassC::class, $class);
    }

    public function testUnknownDependency()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Unknown dependency someDep');
        $this->container->getDependencyByName('someDep');
    }
}
