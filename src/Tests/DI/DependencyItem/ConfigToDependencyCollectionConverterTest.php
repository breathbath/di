<?php

namespace B\Tests\DI\DependencyItem;

use B\DI\DependencyItem\ConfigFields;
use B\DI\DependencyItem\ConfigToDependencyCollectionConverter;
use B\DI\DependencyItem\DependencyItem;
use B\DI\DependencyItem\DependencyItemCollection;
use B\DI\DependencyItem\DependencyItemStatic;
use B\Examples\Classes\ClassA;
use B\Examples\Classes\ClassB;
use B\Examples\Classes\ClassC;
use B\Examples\Classes\ClassD;

class ConfigToDependencyCollectionConverterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var array
     */
    private $config;

    protected function setUp()
    {
        $this->config = [
            [
                ConfigFields::CLASS_FIELD => ClassA::class,
                ConfigFields::DEPENDENCIES_FIELD => [
                    ClassB::class
                ]
            ],
            [
                ConfigFields::CLASS_FIELD => ClassD::class,
                ConfigFields::DEPENDENCIES_FIELD => [
                    ClassA::class,
                    ClassC::class
                ]
            ],
            [
                ConfigFields::CLASS_FIELD => ClassB::class,
                ConfigFields::DEPENDENCIES_FIELD => [
                    ClassC::class
                ]
            ],
            [
                ConfigFields::CLASS_FIELD => ClassC::class
            ],

        ];
    }

    public function testCollectionCreation()
    {
        $converter = new ConfigToDependencyCollectionConverter($this->config);
        $collection = $converter->getCollection();

        $expectedCollection = [
            [
                'class' => ClassA::class,
                'closure' => $this->getClosure(ClassA::class, [ClassB::class]),
                'dependencies' => [ClassB::class]
            ],
            [
                'class' => ClassD::class,
                'closure' => $this->getClosure(ClassD::class, [ClassA::class, ClassC::class]),
                'dependencies' => [ClassA::class, ClassC::class]
            ],
            [
                'class' => ClassB::class,
                'closure' => $this->getClosure(ClassB::class, [ClassC::class]),
                'dependencies' => [ClassC::class]
            ],
            [
                'class' => ClassC::class,
                'closure' => $this->getClosure(ClassC::class, []),
                'dependencies' => []
            ],
        ];

        $this->assertEquals($expectedCollection, $this->convertDependencyCollectionToArray($collection));
    }

    /**
     * @param string $class
     * @param array $dependencies
     * @return \Closure
     */
    private function getClosure($class, $dependencies)
    {
        $item = new DependencyItemStatic($class, $dependencies);
        return $item->getClosure();
    }

    /**
     * @param DependencyItem $dependencyItem
     * @return array
     */
    private function convertDependencyItemToArray(DependencyItem $dependencyItem)
    {
        return [
            'class' => $dependencyItem->getClass(),
            'closure' => $dependencyItem->getClosure(),
            'dependencies' => $dependencyItem->getDependencyNames()
        ];
    }

    /**
     * @param DependencyItemCollection $collection
     * @return array
     */
    private function convertDependencyCollectionToArray(DependencyItemCollection $collection)
    {
        $result = [];
        foreach ($collection as $item) {
            $result[] = $this->convertDependencyItemToArray($item);
        }
        return $result;
    }
}