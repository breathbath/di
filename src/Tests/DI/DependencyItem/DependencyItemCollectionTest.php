<?php

namespace B\Tests\DI\DependencyItem;

use B\DI\DependencyItem\DependencyItem;
use B\DI\DependencyItem\DependencyItemCollection;

class DependencyItemCollectionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var DependencyItemCollection
     */
    private $collection;

    protected function setUp()
    {
        $this->collection = new DependencyItemCollection();
    }

    public function testCollectionHoldingData()
    {
        $dep1 = $this->prophesize(DependencyItem::class)->reveal();
        $dep2 = $this->prophesize(DependencyItem::class)->reveal();

        $collection = $this->collection;

        $this->assertEquals(0, $collection->count());

        $collection->attach($dep1);
        $collection->attach($dep2);
        $this->assertEquals(2, $collection->count());

        $this->assertTrue($collection->contains($dep1));
        $this->assertTrue($collection->contains($dep2));
    }

    public function testAttachingCorrectClass()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessageRegExp('/^' . preg_quote(DependencyItem::class) . '/');

        $someWrongClass = new \stdClass();
        $this->collection->attach($someWrongClass);
    }

    public function testAttachingCorrectCollectionInstance()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessageRegExp('/^' . preg_quote(DependencyItemCollection::class) . '/');

        $someWrongClass = new \stdClass();
        $this->collection->addAll($someWrongClass);
    }
}
