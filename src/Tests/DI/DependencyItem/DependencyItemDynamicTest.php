<?php

namespace B\Tests\DI\DependencyItem;

use B\DI\DependencyItem\DependencyItemDynamic;
use B\DI\DependencyItem\DependencyItemStatic;
use B\Examples\Classes\ClassB;
use B\Examples\Classes\ClassC;

class DependencyItemDynamicTest extends \PHPUnit_Framework_TestCase
{
    public function testHoldingVariables()
    {
        $closure = function () {
            return 'abc';
        };
        $dependencyItem = new DependencyItemDynamic('oneClass', $closure, ['dependency1']);
        $this->assertEquals('oneClass', $dependencyItem->getClass());
        $this->assertEquals(['dependency1'], $dependencyItem->getDependencyNames());
        $this->assertEquals($closure, $dependencyItem->getClosure());
        $this->assertEquals('abc', $closure->__invoke());
    }
}