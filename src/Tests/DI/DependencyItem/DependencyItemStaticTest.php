<?php

namespace B\Tests\DI\DependencyItem;

use B\DI\DependencyItem\DependencyItemStatic;
use B\Examples\Classes\ClassB;
use B\Examples\Classes\ClassC;

class DependencyItemStaticTest extends \PHPUnit_Framework_TestCase
{
    public function testHoldingVariables()
    {
        $dependencyItem = new DependencyItemStatic('someClass', ['dep1', 'dep2']);
        $this->assertEquals('someClass', $dependencyItem->getClass());
        $this->assertEquals(['dep1', 'dep2'], $dependencyItem->getDependencyNames());
    }

    public function testClosureGeneration()
    {
        $dependencyItem = new DependencyItemStatic(ClassB::class, [ClassC::class]);
        $closure = $dependencyItem->getClosure();
        $classB = $closure->__invoke(new ClassC());
        $this->assertInstanceOf(ClassB::class, $classB);
    }
}