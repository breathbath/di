<?php

namespace B\Tests\DI\DependencyItem;

use B\DI\DependencyItem\DependencyItem;
use B\DI\DependencyItem\DynamicItemCollectionProvider;
use B\DI\DependencyItem\DynamicItemFactory;
use Prophecy\Prophecy\ObjectProphecy;

class DynamicItemCollectionProviderTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var DynamicItemFactory|ObjectProphecy
     */
    private $factory;

    /**
     * @var DynamicItemCollectionProvider
     */
    private $dynamicItemCollectionProvider;

    protected function setUp()
    {
        $someItem = $this->prophesize(DependencyItem::class);
        $someItem->getClass()->willReturn('someClass');

        $this->factory = $this->prophesize(DynamicItemFactory::class);
        $this->factory->createDynamicItem()->willReturn($someItem->reveal());

        $this->dynamicItemCollectionProvider = new DynamicItemCollectionProvider();
    }

    public function testItemsCollectionCreation()
    {
        $this->dynamicItemCollectionProvider->addDynamicItemFactory($this->factory->reveal());
        $collection = $this->dynamicItemCollectionProvider->getCollection();
        $this->assertEquals(1, $collection->count());
        foreach ($collection as $item) {
            $this->assertEquals('someClass', $item->getClass());
        }
    }

}